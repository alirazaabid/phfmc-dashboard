﻿using HPM.Models.ViewModels;
using HPM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.IO;

namespace HPM.Controllers
{
    public class ProjectsController : Controller
    {
        private readonly ProjectServices _services;
        public ProjectsController()
        {
            _services = new ProjectServices();
        }
        // GET: Projects
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadProjects()
        {
            var list = _services.LoadProject();
            return PartialView("~/Views/Projects/_LoadProjects.cshtml", list);
        }

        [HttpPost]
        public ActionResult LoadProjectsDetail(decimal? id)
        {
            var pro = _services.LoadProjectDetail(id);
            return Json(pro);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public JsonResult Save(string model)
        {
            var projectModel = JsonConvert.DeserializeObject<ProjectModel>(model);
            FetchImage(projectModel);
            var userId = User.Identity.GetUserId();
            var isSave = _services.SaveProject(projectModel, userId);
            return Json(isSave);
        }

        private void FetchImage(ProjectModel projectModel)
        {
            if (Request.Files.Count!=0)
            {
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];
                    var path = Server.MapPath("~/Images/Project/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    var random = new Random().Next(5555555);
                    var fileName =  random+ "-"+ DateTime.Now.ToString("ddMMyyyhhmmss") + "-"+file.FileName;
                    projectModel.ImageName = fileName;

                    var filePath = Path.Combine(path , fileName);
                    file.SaveAs(filePath);
                }
            }
        }

        //[HttpPost]
        //public JsonResult Save(ProjectModel models)
        //{
        //    var userId = User.Identity.GetUserId();
        //    var isSave = _services.SaveProject(models,userId);
        //    return Json(isSave);
        //}

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public JsonResult Edit(string model)
        {
            var projectModel = JsonConvert.DeserializeObject<ProjectModel>(model);
            FetchImage(projectModel);
            var userId = User.Identity.GetUserId();
            var isSave = _services.EditProject(projectModel, userId);
            return Json(isSave);
        }

        public ActionResult Detail(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var proId = Convert.ToDecimal(id);
                var model = _services.LoadProjectDetail(proId);
                return View(model);
            }
            else
                return View("Error");
           
        }

    }
}