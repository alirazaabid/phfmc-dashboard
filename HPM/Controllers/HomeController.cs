﻿using HPM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Text;

namespace HPM.Controllers
{
    public class HomeController : Controller
    {
        private readonly HomeService _service;
        public HomeController()
        {
            _service = new HomeService();
        }

        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var list = _service.LoadUserProject(userId);
            return View(list);
        }

        [HttpPost]
        public JsonResult LoadProjectCount()
        {
            var count = _service.ProjectCount();
            return Json(count);
        }

        [HttpPost]
        public JsonResult LoadUserCount()
        {
            var count = _service.UserCount();
            return Json(count);
        }

        [HttpPost]
        public JsonResult LoadTeamLeadCount()
        {
            var count = _service.TeamLeadCount();
            return Json(count);
        }

        [HttpPost]
        public JsonResult LoadDeveloperCount()
        {
            var count = _service.DeveloperCount();
            return Json(count);
        }

        [HttpPost]
        public JsonResult LoadUserProject()
        {
            var userId = User.Identity.GetUserId();
            var list = _service.LoadUserProject(userId);
            return Json(list);
        }
    }
}