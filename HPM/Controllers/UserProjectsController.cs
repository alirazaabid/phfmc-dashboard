﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HPM.Services;
using HPM.Models.ViewModels;
using Microsoft.AspNet.Identity;


namespace HPM.Controllers
{
    public class UserProjectsController : Controller
    {
        private readonly UserProjectService _service;

        // GET: UserProjects
        public ActionResult Index()
        {
            return View();
        }

        public UserProjectsController()
        {
            _service = new UserProjectService();
        }

        [HttpPost]
        public ActionResult LoadUserProjects()
        {
            var list = _service.LoadUserProject();
            return PartialView("~/Views/UserProjects/_LoadUserProjects.cshtml", list);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Save(UserProjectModel models)
        {
            var userId = User.Identity.GetUserId();
            var isSave = _service.SaveUserProject(models, userId);
            return Json(isSave);
        }

        [HttpPost]
        public JsonResult Delete(decimal? id)
        {
            var isDeleted = _service.DeleteUserProject(id);
            return Json(isDeleted);
        }

    }
}