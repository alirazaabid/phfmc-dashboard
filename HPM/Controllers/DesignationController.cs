﻿using HPM.Models.ViewModels;
using HPM.Services;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HPM.Controllers
{
    public class DesignationController : Controller
    {
        private readonly DesignationService _service;


        public DesignationController()
        {
            _service = new DesignationService();
        }
        // GET: Designation
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadDesignations()
        {
            var list = _service.LoadDesignations();
            return PartialView("~/Views/Designation/_LoadDesignation.cshtml", list);
        }

        [HttpPost]
        public ActionResult LoadDesignationDetail(decimal? id)
        {
            var developer = _service.LoadDesignationDetail(id);
            return Json(developer);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Save(DesignationModel models)
        {
            var userId = User.Identity.GetUserId();
            var isSave = _service.SaveDesignation(models, userId);
            return Json(isSave);
        }


        [HttpGet]
        public ActionResult Edit(string id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public JsonResult Edit(DesignationModel models)
        {
            var userId = User.Identity.GetUserId();
            var isSave = _service.EditDesignation(models, userId);
            return Json(isSave);
        }



    }
}