﻿using HPM.Models.ViewModels;
using HPM.Services;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HPM.Controllers
{
    public class ProgramController : Controller
    {
        private readonly ProgramService _service;

        public ProgramController()
        {
            _service = new ProgramService();
        }
        // GET: Program
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPrograms()
        {
            var list = _service.LoadPrograms();
            return PartialView("~/Views/Program/_LoadPrograms.cshtml", list);
        }

        [HttpPost]
        public ActionResult LoadProgramDetail(decimal? id)
        {
            var pro = _service.LoadProgramDetail(id);
            return Json(pro);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Save(ProgramModel model)
        {
            var userId = User.Identity.GetUserId();
            var isSave = _service.SaveProgram(model, userId);
            return Json(isSave);
        }


        [HttpGet]
        public ActionResult Edit(string id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public JsonResult Edit(ProgramModel model)
        {
            var userId = User.Identity.GetUserId();
            var isSave = _service.EditProgram(model, userId);
            return Json(isSave);
        }

        [HttpPost]
        public ActionResult Disable(decimal? id)
        {
            var isDisable = _service.DisableProgram(id);
            return Json(isDisable);
        }

        //[HttpGet]
        //public ActionResult Projects()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult LoadProjects()
        //{
        //    var list = _service.LoadProjects();
        //    return PartialView("~/Views/Program/_LoadProjects.cshtml", list);
        //}

    }
}