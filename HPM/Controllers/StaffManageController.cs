﻿
using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using System.Collections.Generic;
using HPM;
using HPM.Models;
using HPM.Models;

namespace HPM.Controllers
{
    public class StaffManageController : Controller
    {
        HPMDBEntities db = new HPMDBEntities();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        PasswordHasher passwordHasher = new PasswordHasher();

        /// <summary>
        /// Users
        /// </summary>
        /// <returns></returns>
        // GET: StaffManage
        public ActionResult Users()
        {
            return View();
        }

        public ActionResult UsersView()
        {
            //var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            //var list = userManager.Users.ToList();

          
            var list = db.AspNetUsers.ToList();
            return PartialView("~/Views/StaffManage/_UsersView.cshtml", list);
        }

        public ActionResult AddUser()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            ViewBag.roles = roleManager.Roles.ToList();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddUser(CreateUserViewModel model)
        {
            try
            {


                ApplicationDbContext context = new ApplicationDbContext();
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                ViewBag.roles = roleManager.Roles.ToList();


                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email, IsActive = true, Hashnoty = model.Password, UserDetail = model.UserDetail };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    foreach (var role in model.roles)
                    {
                        if (!string.IsNullOrEmpty(role))
                        {
                            UserManager.AddToRole(user.Id, role);
                        }
                    }

                    return RedirectToAction("Users", "StaffManage");
                }
                AddErrors(result);

            }
            catch (Exception ex)
            {
                throw;
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public ActionResult EditUser(string id)
        {

            // Get the existing student from the db
            var user = UserManager.FindById(id);

            ApplicationDbContext context = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            UpdateUserViewModel model = new UpdateUserViewModel();
            if (user.Roles.Any())
            {
                foreach (var role in user.Roles)
                {
                    //model.roles.Add(roleManager.FindById(role?..RoleId).Name);
                    model.roles.Add(role?.ToString());

                }
            }


            model.Email = user.Email;
            model.Id = user.Id;
            model.UserName = user.UserName;

            if (!string.IsNullOrEmpty(user.UserDetail))
            {
                model.UserDetail = user.UserDetail;
            }

            ViewBag.roles = roleManager.Roles.ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult EditUser(UpdateUserViewModel model)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            ViewBag.roles = roleManager.Roles.ToList();

            // Get the existing student from the db
            var user = UserManager.FindById(model.Id);

            // Update it with the values from the view model
            if (!string.IsNullOrEmpty(model.UserName))
            {
                user.UserName = model.UserName;
            }
            if (!string.IsNullOrEmpty(model.Email))
            {
                user.Email = model.Email;
            }
            if (!string.IsNullOrEmpty(model.UserDetail))
            {
                user.UserDetail = model.UserDetail;
            }


            if (!string.IsNullOrEmpty(model.Password))
            {
                user.PasswordHash = passwordHasher.HashPassword(model.Password);
                user.Hashnoty = model.Password;
            }

            user.IsActive = true;

            if (model.roles.Any())
            {
                if (user.Roles.Any())
                {
                    //model.roles.Add(roleManager.FindById(role?..RoleId).Name);
                    var userRoles = user.Roles.Select(x => roleManager.FindById(x.RoleId).Name).ToArray();
                    //var userRoles = user.Roles.Select(x => x.ToString()).ToArray();
                    UserManager.RemoveFromRoles(model.Id, userRoles);
                }

                UserManager.Update(user);

                foreach (var role in model.roles)
                {
                    if (!string.IsNullOrEmpty(role))
                    {
                        UserManager.AddToRole(user.Id, role);
                    }
                }
            }

            // Apply the changes if any to the db
            var result = UserManager.Update(user);
            if (result.Succeeded)
            {
                return RedirectToAction("Users", "StaffManage");
            }

            AddErrors(result);


            return View();
        }


        /// <summary>
        /// Roles
        /// </summary>
        /// <returns></returns>
        ///  
        public ActionResult Roles()
        {
            return View();
        }
        public ActionResult RolesView()
        {

            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var roles = roleManager.Roles.ToList();
            return PartialView("~/Views/StaffManage/_RolesView.cshtml", roles);
        }

        public string SaveRole(string nameParam)
        {
            string res = "false";

            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!roleManager.RoleExists(nameParam))
            {
                var role = new IdentityRole();
                role.Name = nameParam;
                roleManager.Create(role);
                return "true";
            }
            else
            {
                return res;
            }
        }

        //public async Task<string> CopyUsers()
        //{
        //    var res = "false";
        //    try
        //    {
        //        foreach (var u in db.Users)
        //        {
        //            ApplicationDbContext context = new ApplicationDbContext();
        //            var roleStore = new RoleStore<IdentityRole>(context);
        //            var roleManager = new RoleManager<IdentityRole>(roleStore);
        //            ViewBag.roles = roleManager.Roles.ToList();

        //            var user = new ApplicationUser
        //            {
        //                UserName = u.UserName,
        //                Email = "123@abc.com",
        //                LevelID = u.LevelID,
        //                UserDetail = u.UserDetail,
        //                CreationDate = DateTime.UtcNow.AddHours(5),
        //                HfmisCodeNew = u.HfmisCodeNew,
        //                hashynoty = u.Password,
        //                isActive = true
        //            };

        //            var result = await UserManager.CreateAsync(user, u.Password);
        //            if (result.Succeeded)
        //            {
        //                if (u.LevelID == 4)
        //                {
        //                    UserManager.AddToRole(user.Id, "Health Facility");
        //                }
        //                else
        //                {
        //                    UserManager.AddToRole(user.Id, "Administrator");
        //                }
        //            }
        //        }
        //        res = "true";
        //        return res;
        //    }
        //    catch (Exception e)
        //    {
        //        return e.ToString();
        //    }
        //}

        public void deleteRole(string nameParam)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (roleManager.RoleExists(nameParam))
            {
                var role = roleManager.FindByName(nameParam);
                roleManager.Delete(role);
            }
        }


        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public ActionResult GetUsers(string roleName)
        {

            List<ApplicationUser> users = new List<ApplicationUser>();

            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var role = db.Roles.FirstOrDefault(r => r.Name == roleName);
                    if (role == null) return Json(users, JsonRequestBehavior.AllowGet);
                    var usersList = db.Users.Where(x => x.Roles.Any(r => r.RoleId == role.Id)).ToList();

                    foreach (var item in usersList)
                    {
                        users.Add(new ApplicationUser() { Id = item.Id, UserName = item.UserName });
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return Json(users, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}