﻿using HPM.Models.ViewModels;
using HPM.Services;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HPM.Controllers
{
    public class DeveloperController : Controller
    {
        private readonly DeveloperService _service;
        // GET: Developer
        public ActionResult Index()
        {
            return View();
        }

        public DeveloperController()
        {
            _service = new DeveloperService();
        }

        [HttpPost]
        public ActionResult LoadDevelopers()
        {
            var list = _service.LoadDevelopers();
            return PartialView("~/Views/Developer/_LoadDevelopers.cshtml", list);
        }

        [HttpPost]
        public ActionResult LoadDeveloperDetail(decimal? id)
        {
            var developer = _service.LoadDeveloperDetail(id);
            return Json(developer);
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Save(DeveloperModel models)
        {
            var userId = User.Identity.GetUserId();
            var isSave = _service.SaveDeveloper(models, userId);
            return Json(isSave);
        }


        [HttpGet]
        public ActionResult Edit(string id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public JsonResult Edit(DeveloperModel models)
        {
            var userId = User.Identity.GetUserId();
            var isSave = _service.EditProject(models, userId);
            return Json(isSave);
        }



    }
}