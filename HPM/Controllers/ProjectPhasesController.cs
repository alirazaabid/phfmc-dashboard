﻿using HPM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HPM.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace HPM.Controllers
{
    public class ProjectPhasesController : Controller
    {

        private readonly ProjectIterationService _service;

        public ProjectPhasesController()
        {
            _service = new ProjectIterationService();
        }
        // GET: ProjectPhases
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadProjectsIterations()
        {
            var list = _service.LoadProjectIteration();
            return PartialView("~/Views/ProjectPhases/_LoadProjectsIterations.cshtml", list);
        }

        [HttpPost]
        public ActionResult LoadProjectIterationsDetail(decimal? id)
        {
            var pro = _service.LoadProjectIterationDetail(id);
            return Json(pro);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Save(ProjectIterationModel model)
        {
            var userId = User.Identity.GetUserId();
            var isSave = _service.SaveProjectIteration(model, userId);
            return Json(isSave);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public JsonResult Edit(ProjectIterationModel model)
        {
            var userId = User.Identity.GetUserId();
            var isSave = _service.EditProjectIteration(model, userId);
            return Json(isSave);
        }

        public ActionResult Detail(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var proId = Convert.ToDecimal(id);
                var model = _service.LoadProjectIterationDetail(proId);
                return View(model);
            }
            else
                return View("Error");

        }
    }
}