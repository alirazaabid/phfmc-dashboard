﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HPM.Models;
using HPM.Services;

namespace HPM.Controllers
{
    public class DrpDownController : Controller
    {
        // GET: DrpDown
        private readonly DropDownService _services;
        public DrpDownController()
        {
            _services = new DropDownService();
        }

        [HttpPost]
        public JsonResult LoadUsers()
        {
           var list = _services.LoadUsers();
            return Json(list);
        }

        [HttpPost]
        public JsonResult LoadProject()
        {
            var list = _services.LoadProjects();
            return Json(list);
        }

        [HttpPost]
        public JsonResult LoadProjectRights()
        {
            var list = _services.LoadProjectRights();
            return Json(list);
        }

        [HttpPost]
        public JsonResult LoadTechnologies()
        {
            var list = _services.LoadTechnologies();
            return Json(list);
        }

        [HttpPost]
        public JsonResult LoadDevelopers()
        {
            var list = _services.LoadDevelopers();
            return Json(list);
        }

        [HttpPost]
        public JsonResult LoadTeamLeads()
        {
            var list = _services.LoadTeamLeads();
            return Json(list);
        }

        [HttpPost]
        public JsonResult LoadDesignations()
        {
            var list = _services.LoadDesignations();
            return Json(list);
        }

        [HttpPost]
        public JsonResult LoadPrograms()
        {
            var list = _services.LoadPrograms();
            return Json(list);
        }
    }
}