﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HPM.Startup))]
namespace HPM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
