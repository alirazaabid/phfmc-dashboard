﻿angular.module("IDUFileApp").requires.push('ngSanitize');


app.controller("browserCtrl", function ($scope, fileService, browserService, $http) {
    $scope.isLoading = false;
    $scope.FolderList = null;
    $scope.FileList = null;

    $scope.fetchFolders = function (id) {
        browserService.getFoldersDrp(id).then(function (data) {
            $scope.FolderList = data;
            console.log($scope.FolderList);
        });
    };

    $scope.fetchFiles = function (id) {
        browserService.FetchFiles(id).then(function (data) {
            $scope.FileList = data;
            console.log($scope.FileList);
        });
    };

    $scope.OnDbleClickFolder = function(selctFldr) {
        document.getElementById("hidFolderId").value = selctFldr.Id;
        $scope.getFolderFiles();
    };

    $scope.OnDbleClickFile = function (file) {
        window.open("/File/Download?id=" + file.Id);
    }; 

    angular.element(document).ready(function () {
        $scope.getFolderFiles();
    });

    $scope.getFolderFiles = function() {
        var id = document.getElementById("hidFolderId").value;
        $scope.fetchFolders(id);
        if (id != null && id !== "") {
            $scope.fetchFiles(id);
        }
    };

});