﻿$(document).ready(function () {
  loadProjectsPhases();
});

var loadProjectsPhases = function () {
  var jqXHR = $.ajax({
    url: "/ProjectPhases/LoadProjectsIterations",
    type: "POST",
  }).done(function (res) {
    $("#divPartialView").html(res);
    var t = $('#tbl').DataTable({
      "ordering": false,
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      },
      {
        "searchable": false,
        "orderable": false,
        "targets": 6
      }],
      "order": [[1, 'asc']]
    });
    t.on('order.dt search.dt', function () {
      t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw();
  });
};