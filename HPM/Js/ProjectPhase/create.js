﻿angular.module("HpmApp").requires.push('ngSanitize');
angular.module("HpmApp").requires.push('ui.select');
//angular.module("HpmApp").requires.push("ui.bootstrap");
angular.module("HpmApp").requires.push("ngMaterial");

app.controller("createProjectPhaseCtrl", function ($scope, $http) {

  $scope.proIteration = {
    Name: "",
    Description: "",
    StartDate: null,
    EndDate: null,
    Project: null
  };

  angular.element(document).ready(function () {
    $scope.loadProjects();
  });

  $scope.loadProjects = function () {
    $http({
      url: "/DrpDown/LoadProject",
      method: "POST"
    }).then(function (res) {
      $scope.Projects = res.data;

    });
  };

  $scope.save = function () {
    $http({
      url: "/ProjectPhases/Save",
      method: "POST",
      data: $scope.proIteration
    }).then(function (res) {
      if (res.data === "true") {
        $scope.reset();
        toastr.success('Project Phase added successsfully...');

      } else {
        toastr.error(res.data);
      }

    });
  };

  $scope.reset = function () {
    $scope.frm.$setPristine();
    $scope.frm.$setUntouched();
    $scope.proIteration = {

    };
  };


  /////// date picker pop up start ///////

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  $scope.format = "dd-MM-yyyy";

  $scope.open1 = function () {
    $scope.popup1.opened = true;
  };

  $scope.popup1 = {
    opened: false
  };

  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }

    ///////// date picker pop up end ///////

  /// toaster setting///
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

});