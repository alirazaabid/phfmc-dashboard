﻿angular.module("HpmApp").requires.push('ngSanitize');
angular.module("HpmApp").requires.push('ui.select');
angular.module("HpmApp").requires.push("ngMaterial");

app.controller("editProjectPhaseCtrl", function ($scope, $http, commonService) {

  $scope.proIteration = {
    Name: "",
    Description: "",
    StartDate: null,
    EndDate: null,
    Project: null
  };

  angular.element(document).ready(function () {
    $scope.loadDesignationDetail();
    $scope.loadProjects();
  });

  $scope.loadDesignationDetail = function () {
    var id = $("#hidId").val();
    console.log(id);
    if (id !== "") {
      $http({
        url: "/ProjectPhases/LoadProjectIterationsDetail",
        method: "POST",
        data: { id: id }
      }).then(function (res) {
        $scope.proIteration = res.data;
        $scope.proIteration.StartDate = commonService.convertToDate(res.data.StartDate);
        $scope.proIteration.EndDate = commonService.convertToDate(res.data.EndDate)
      });
    }

  }
  $scope.loadProjects = function () {
    $http({
      url: "/DrpDown/LoadProject",
      method: "POST"
    }).then(function (res) {
      $scope.Projects = res.data;

    });
  };

  $scope.save = function () {
    $http({
      url: "/ProjectPhases/Edit",
      method: "POST",
      data: $scope.proIteration
    }).then(function (res) {
      if (res.data === "true") {
        $scope.reset();
        toastr.success('Project Phase updated successsfully...');

      } else {
        toastr.error(res.data);
      }

    });
  };

  $scope.reset = function () {
    $scope.frm.$setPristine();
    $scope.frm.$setUntouched();
    $scope.proIteration = {

    };
  };


  
  /// toaster setting///
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

});