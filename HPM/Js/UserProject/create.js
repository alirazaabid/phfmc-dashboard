﻿angular.module("HpmApp").requires.push('ngSanitize');
angular.module("HpmApp").requires.push('ui.select');

app.controller("createUserProjectCtrl", function ($scope, $http) {

  angular.element(document).ready(function () {
    $scope.loadUsers();
    $scope.loadProjects();
    $scope.loadProjectRights();
  });

  $scope.selUserProject = {};

  $scope.loadUsers = function () {
    $http({
      url: "/DrpDown/LoadUsers",
      method: "POST"
    }).then(function (res) {
      $scope.users = res.data;

    });
  };

  $scope.loadProjects = function () {
    $http({
      url: "/DrpDown/LoadProject",
      method: "POST"
    }).then(function (res) {
      console.log()
      $scope.projects = res.data;

    });
  };

  $scope.loadProjectRights = function () {
    $http({
      url: "/DrpDown/LoadProjectRights",
      method: "POST"
    }).then(function (res) {
      $scope.projectRights = res.data;

    });
  };

  $scope.save = function () {
    $http({
      url: "/UserProjects/Save",
      method: "POST",
      data: $scope.selUserProject
    }).then(function (res) {
      if (res.data === "save") {
        $scope.reset();
        toastr.success('User Project added successsfully.');
      } else if (res.data === "update") {
        $scope.reset();
        toastr.success('User Project updated successsfully.');
      }
      else {
        toastr.error(res.data);
      }

    });
  }


  $scope.reset = function () {
    $scope.frm.$setPristine();
    $scope.frm.$setUntouched();
    $scope.selUserProject = {};
  };


  /// toaster setting///
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

});



