﻿$(document).ready(function () {
  loadUserProjects();
});

var loadUserProjects = function () {
  var jqXHR = $.ajax({
    url: "/UserProjects/LoadUserProjects",
    type: "POST",
  }).done(function (res) {
    $("#divPartialView").html(res);
   var t = $('#tblUserProject').DataTable( {
     "columnDefs": [
       {
            "searchable": false,
            "orderable": false,
            "targets": 0
       },
       {
         "searchable": false,
         "orderable": false,
         "targets":5
       }
     ],
        "order": [[ 1, 'asc' ]]
    } );
 
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
  });
};

var deleteUserPro = function (id) {
  var aa = confirm("Are you sure you want to delete.");
  if (aa) {
    var jqXHR = $.ajax({
      url: "/UserProjects/Delete",
      type: "POST",
      data: { id: id },
    }).done(function (res) {
      if (res === "true") {
        toastr.success('User Project deleted successsfully...');
        loadUserProjects();
      } else {
        toastr.error(res);
      }

    });
  }

}



/// toaster setting///
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};