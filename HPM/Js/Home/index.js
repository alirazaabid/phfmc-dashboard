﻿app.controller("homeCtrl", function ($scope, $http) {

  angular.element(document).ready(function () {
    $scope.loadProjectCount();
    $scope.loadUserCount();
    $scope.loadTeamLeadCount();
    $scope.loadDeveloperCount();
  
  });
  $scope.projectCount = 0;
  $scope.userCount = 0;
  $scope.teamLeasdCount = 0;
  $scope.developerCount = 0;

  $scope.loadProjectCount = function () {
    $http({
      url: "/Home/LoadProjectCount",
      method: "POST",
    }).then(function (res) {
      $scope.projectCount = res.data;
    });
  };

  $scope.loadUserCount = function () {
    $http({
      url: "/Home/LoadUserCount",
      method: "POST",
    }).then(function (res) {
      $scope.userCount = res.data;
    });
  };


  $scope.loadTeamLeadCount = function () {
    $http({
      url: "/Home/LoadTeamLeadCount",
      method: "POST",
    }).then(function (res) {
      $scope.teamLeasdCount = res.data;
    });
  };

  $scope.loadDeveloperCount = function () {
    $http({
      url: "/Home/LoadDeveloperCount",
      method: "POST",
    }).then(function (res) {
      $scope.developerCount = res.data;
    });
  };

  $scope.loadUserProjects = function () {
    $http({
      url: "/Home/LoadUserProject",
      method: "POST",
    }).then(function (res) {
      $scope.userProjects = res.data;
      console.log(res.data);
    });
  };


});