﻿angular.module("HpmApp").requires.push('ngSanitize');
angular.module("HpmApp").requires.push('ui.select');
app.controller("projectCreateCtrl", function ($scope, $http) {

  angular.element(document).ready(function () {
    $scope.loadPrograms();
    $scope.loadTeamLeads();
    $scope.loadDevelopers();
    $scope.loadTechnologies();
  });


  $scope.project = {
    Name: "",
    Description: "",
    Url: "",
    UserName: "",
    Password: "",
    ImageData:"",
    RequestType: null,
    ProjectOrder:"",
    TeamLead: null,
    Program: null,
    Developer: null,
    Technology: null,
  };

  $scope.RequestTypes = ["GET", "POST"];

  $scope.loadPrograms = function () {
    $http({
      url: "/DrpDown/LoadPrograms",
      method: "POST"
    }).then(function (res) {
      $scope.Programs = res.data;

    });
  };

  $scope.loadTeamLeads = function () {
    $http({
      url: "/DrpDown/LoadTeamLeads",
      method: "POST"
    }).then(function (res) {
      $scope.TeamLeads = res.data;

    });
  }

  $scope.loadDevelopers = function () {
    $http({
      url: "/DrpDown/LoadDevelopers",
      method: "POST"
    }).then(function (res) {
      $scope.Developers = res.data;

    });
  }

  $scope.loadTechnologies = function () {
    $http({
      url: "/DrpDown/LoadTechnologies",
      method: "POST"
    }).then(function (res) {
      $scope.Technologies = res.data;

    });
  }

  $scope.save = function () {
    var fd = new FormData();
    var data = _.clone($scope.project);
    fd.append("ProImage", data.ImageData);
    fd.append("model", JSON.stringify(data));
    $http({
      url: "/Projects/Save",
      method: "POST",
      data: fd,
      transformRequest: angular.identity,
      headers: { 'Content-Type': undefined }
    }).then(function (res) {
      if (res.data === "true") {
        $scope.reset();
        toastr.success('Project added successsfully...');

      } else {
        toastr.error(res.data);
      }

    });
  };

  $scope.reset = function () {
    $scope.frm.$setPristine();
    $scope.frm.$setUntouched();
    $scope.project = {
      Name: "",
      Description: "",
      Url: "",
      UserName: "",
      Password: "",
    };
  };



  /// toaster setting///
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

});