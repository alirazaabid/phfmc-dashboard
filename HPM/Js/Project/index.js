﻿$(document).ready(function () {
  loadProjects();
});

var loadProjects = function () {
  var jqXHR = $.ajax({
    url: "/Projects/LoadProjects",
    type: "POST",
  }).done(function (res) {
    $("#divPartialView").html(res);
    var t = $('#tblProject').DataTable({
      "ordering": false,
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      },
        {
          "searchable": false,
          "orderable":false,
          "targets":6
      }],
      "order": [[1, 'asc']]
    });
    t.on('order.dt search.dt', function () {
      t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw();
  });
};