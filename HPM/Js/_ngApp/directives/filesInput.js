﻿app.directive("filesInput", function () {
    return {
        require: "ngModel",
        link: function postLink(scope, elem, attrs, ngModel) {
            elem.on("change", function (e) {
                var file = elem[0].files[0];
                debugger;
                ngModel.$setViewValue(file);

                var reader = new FileReader();
                if (document.getElementById(attrs.filesImageId)) {
                    reader.onload = function () {
                        var dataUrl = reader.result;
                        var output = document.getElementById(attrs.filesImageId);
                        if (output) {
                            output.src = dataUrl;
                        }
                    };
                }
                reader.readAsDataURL(file);
            });
        }
    }
});