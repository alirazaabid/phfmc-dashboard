﻿app.factory("browserService", ["$http", function($http) {
    return {
        
        FetchFiles: function (fldr) {
            var url = "/File/FetchFilesBrowser";
            return $http({
                url: url,
                data: { folderId: fldr },
                method: "POST"
            }).then(function (res) {
                return res.data;
            });
        },
        getFoldersDrp: function (id) {
            var url = "/Folder/FetchFolders";
            return $http({
                url: url,
                data: { id: id },
                method: "POST"
            }).then(function (res) {
                return res.data;
            });
        }
    }

    }
]);