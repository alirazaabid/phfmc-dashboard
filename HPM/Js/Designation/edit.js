﻿app.controller("editDestCtrl", function ($scope, $http) {

  angular.element(document).ready(function () {
    $scope.loadDesignationDetail();
  });


  $scope.dest = {
    Name: "",
    Description: "",
  };

  $scope.loadDesignationDetail = function () {
    var id = $("#hidId").val();
    console.log(id);
    if (id !== "") {
      $http({
        url: "/Designation/LoadDesignationDetail",
        method: "POST",
        data: { id: id }
      }).then(function (res) {
        $scope.dest = res.data;
      });
    }

  }


  $scope.save = function () {
    $http({
      url: "/Designation/Edit",
      method: "POST",
      data: $scope.dest
    }).then(function (res) {
      if (res.data === "true") {
        $scope.reset();
        toastr.success('Designation updated successsfully...');

      } else {
        toastr.error(res.data);
      }

    });
  };

  $scope.reset = function () {
    $scope.frm.$setPristine();
    $scope.frm.$setUntouched();
    $scope.dest = {

    };
  };



  /// toaster setting///
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

});