﻿app.controller("createDestCtrl", function ($scope, $http) {
  $scope.dest = {
    Name: "",
    Description: "",
  };


  $scope.save = function () {
    $http({
      url: "/Designation/Save",
      method: "POST",
      data: $scope.dest
    }).then(function (res) {
      if (res.data === "true") {
        $scope.reset();
        toastr.success('Designation added successsfully...');

      } else {
        toastr.error(res.data);
      }

    });
  };

  $scope.reset = function () {
    $scope.frm.$setPristine();
    $scope.frm.$setUntouched();
    $scope.dest = {

    };
  };



  /// toaster setting///
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

});