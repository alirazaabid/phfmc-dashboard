﻿$(document).ready(function () {
  loadDesignations();
});

var loadDesignations = function () {
  var jqXHR = $.ajax({
    url: "/Designation/LoadDesignations",
    type: "POST",
  }).done(function (res) {
    $("#divPartialView").html(res);
    var t = $('#tbl').DataTable({
      "ordering": false,
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      },
      {
        "searchable": false,
        "orderable": false,
        "targets": 2
      }],
      "order": [[1, 'asc']]
    });
    t.on('order.dt search.dt', function () {
      t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw();
  });
};