﻿$(document).ready(function () {
    loadPartialView();
});

var loadPartialView = function (pageno) {
    var getUrl = $("#hidPartialViewUrl").val();


    $.ajax({
        url: getUrl,
        type: "POST",
        data: {},
        success: function (msg) {
            $("#divPartialView").html(msg);
            $("#tbl").DataTable();
        },
        error: function () {

        }
    });
};