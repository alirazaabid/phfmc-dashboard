﻿$(document).ready(function () {
    loadPartialView();
});

var loadPartialView = function () {
    var getUrl = $("#hidPartialViewUrl").val();


    $.ajax({
        url: getUrl,
        type: "POST",
        data: {},
        success: function (msg) {
            $("#divPartialView").html(msg);
        },
        error: function () {

        }
    });
}


var showAdd = function (sh) {
    if (sh === "show") {
        $("#divAdd").hide();
        $("#divClose").show();
        $("#divHide").slideDown();
    }
    else {
        $("#divClose").hide();
        $("#divAdd").show();
        $("#divHide").slideUp();
        clears();
    }
}

var clears = function () {
    $("#hidId").val("");
    $("#txtName").val("");
    $("#divClose").hide();
    $("#divAdd").show();
    $("#divHide").slideUp();
}

var editOfficeRole = function (id, name) {
    $("#divAdd").hide();
    $("#divClose").show();
    $("#divHide").slideDown();
    $('html, body').animate({
        scrollTop: $("#divHide").offset().top - 200
    }, 1000);
    $("#hidId").val(id);
    $("#txtName").val(name);
}

var save = function () {
    var getUrl = $("#hidSaveRoleUrl").val();
    var name = $("#txtName").val();
    var er = 0;
    if (name == "") {
        $("#txtName").css("border", "1px solid red");
        er = er + 1;
    }
    else {
        $("#txtName").css("border", "1px solid #ccc");
    }
    if (er == 0) {
        $.ajax({
            url: getUrl,
            type: "POST",
            data: { nameParam: name + "" },
            success: function (response) {
                var message;
                if (response === "true") {
                    loadPartialView();
                    clears();
                } else {
                    message = "Role is already added....";
                }
            },
            error: function () {
            }
        });
    }
}

var deleteRole = function (name) {
    var getUrl = $("#hidDeleteRoleUrl").val();
    var aa = confirm("Are You Sure You Want To Delete?");
    if (aa) {
        $.ajax({
            url: getUrl,
            type: "POST",
            data: { nameParam: name + "" },
            success: function (response) {
                loadPartialView();
            },
            error: function () {
            }
        });
    }
}
