﻿app.controller("editProgramCtrl", function ($scope, $http) {

  angular.element(document).ready(function () {
    $scope.loadProgramDetail();
  });


  $scope.pro = {
    Name: "",
    Description: "",
  };

  $scope.loadProgramDetail = function () {
    var id = $("#hidId").val();
    console.log(id);
    if (id !== "") {
      $http({
        url: "/Program/LoadProgramDetail",
        method: "POST",
        data: { id: id }
      }).then(function (res) {
        $scope.pro = res.data;
      });
    }

  }


  $scope.save = function () {
    $http({
      url: "/Program/Edit",
      method: "POST",
      data: $scope.pro
    }).then(function (res) {
      if (res.data === "true") {
        $scope.reset();
        toastr.success('Program updated successsfully...');

      } else {
        toastr.error(res.data);
      }

    });
  };

  $scope.reset = function () {
    $scope.frm.$setPristine();
    $scope.frm.$setUntouched();
    $scope.pro = {

    };
  };



  /// toaster setting///
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

});