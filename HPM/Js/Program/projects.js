﻿$(document).ready(function () {
  loadProjects();
});

var loadProjects = function () {
  var jqXHR = $.ajax({
    url: "/Program/LoadProjects",
    type: "POST",
  }).done(function (res) {
    $("#divPartialView").html(res);
    var t = $('#tbl').DataTable({
      "ordering": false,
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      },
      {
        "searchable": false,
        "orderable": false,
        "targets": 2
      }],
      "order": [[1, 'asc']]
    });
    t.on('order.dt search.dt', function () {
      t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
      });
    }).draw();
  });
};


var disableProgram = function (id) {
  console.log(id);
  var jqXHR = $.ajax({
    url: "Program/Disable",
    type: "POST",
    data: { id: id }
  }).done(function (res) {
    if (res === "true") {
      loadPrograms();
      toastr.success('Program disabled successsfully...');
    } else {
      toastr.error(res);
    }
    });


};



/// toaster setting///
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};