﻿app.controller("createProgramCtrl", function ($scope, $http) {
  $scope.pro = {
    Name: "",
    Description: "",
  };


  $scope.save = function () {
    $http({
      url: "/Program/Save",
      method: "POST",
      data: $scope.pro
    }).then(function (res) {
      if (res.data === "true") {
        $scope.reset();
        toastr.success('Program added successsfully...');

      } else {
        toastr.error(res.data);
      }

    });
  };

  $scope.reset = function () {
    $scope.frm.$setPristine();
    $scope.frm.$setUntouched();
    $scope.pro = {

    };
  };



  /// toaster setting///
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

});