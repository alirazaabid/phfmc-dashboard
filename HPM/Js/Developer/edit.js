﻿angular.module("HpmApp").requires.push('ngSanitize');
angular.module("HpmApp").requires.push('ui.select');
app.controller("developerEditCtrl", function ($scope, $http) {
  angular.element(document).ready(function () {
    $scope.loadDesignations();
    $scope.loadDeveloperDetail();
  });



  $scope.developer = {
    Name: "",
    LastName: "",
    CNIC: "",
    Address: "",
    ContactNo: "",
    Designation: "",
    Description: {},
    TeamLead: false
  };

  $scope.loadDesignations = function () {
    $http({
      url: "/DrpDown/LoadDesignations",
      method: "POST"
    }).then(function (res) {
      $scope.Designations = res.data;

    });
  }


  $scope.loadDeveloperDetail = function () {
    var id = $("#hidId").val();
    console.log(id);
    if (id !== "") {
      $http({
        url: "/Developer/LoadDeveloperDetail",
        method: "POST",
        data: { id: id }
      }).then(function (res) {
        $scope.developer = res.data;
      });
    }

  }


  $scope.save = function () {
    console.log($scope.developer)
    $http({
      url: "/Developer/Edit",
      method: "POST",
      data: $scope.developer
    }).then(function (res) {
      if (res.data === "true") {
        $scope.reset();
        toastr.success('Developer updated successsfully...');

      } else {
        toastr.error(res.data);
      }

    });
  };

  $scope.reset = function () {
    $scope.frm.$setPristine();
    $scope.frm.$setUntouched();
    $scope.developer = {

    };
  };



  /// toaster setting///
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

});