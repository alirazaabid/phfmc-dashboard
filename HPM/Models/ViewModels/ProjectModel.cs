﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Models.ViewModels
{
    public class ProjectModel
    {
        public ProjectModel()
        {
            Developer = new List<DeveloperModel>();
            ProjectPhases = new List<ProjectIterationModel>();
        }

        public decimal Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ImageName { get; set; }
        public string RequestType { get; set; }
        public int? ProjectOrder { get; set; }
        public string UserNameBase64 { get; set; }
        public string PasswordBase64 { get; set; }
        public ProgramModel Program { get; set; }
        public DeveloperModel TeamLead { get; set; }
        public List<DeveloperModel>  Developer { get; set; }
        public List<ProjectIterationModel> ProjectPhases { get; set; }
        public TechnologyModel Technology { get; set; }

    }
}