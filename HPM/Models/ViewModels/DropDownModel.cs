﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Models.ViewModels
{
    public class DropDownModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}