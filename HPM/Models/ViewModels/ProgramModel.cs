﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Models.ViewModels
{
    public class ProgramModel
    {
        public decimal? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}