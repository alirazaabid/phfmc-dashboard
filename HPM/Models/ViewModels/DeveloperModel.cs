﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Models.ViewModels
{
    public class DeveloperModel
    {
        public decimal? Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string CNIC { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Description { get; set; }
        public bool? TeamLead { get; set; }
        public DesignationModel Designation { get; set; }

    }

}