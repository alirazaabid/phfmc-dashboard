﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Models.ViewModels
{
    public class PaginationViewModel
    {

        public int Draw { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
        public string Search { get; set; }

        public string SortBy { get; set; }
        public int SortIndex { get; set; }
        public int Id { get; set; }
    }

    public class PaginationOptions<T> where T : class
    {
        public PaginationOptions()
        {
            Entities = new List<T>();
        }
        public PaginationOptions(int page, int pageSize, int totalRecords, List<T> entities)
        {
            Page = page;
            PageSize = pageSize;
            TotalRecords = totalRecords;
            Entities = entities;
        }
        public int Draw { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
        public int Pages { get; set; }
        public int NumberSkipped { get; set; }
        public int? NextPage { get; set; }
        public int? PriorPage { get; set; }
        public int FirstPage { get; set; }
        public int LastPage { get; set; }
        public bool OnFirstPage { get; set; }
        public bool OnLastPage { get; set; }
        public bool HasNextPage { get; set; }
        public bool HasPreviousPage { get; set; }
        public List<T> Entities { get; set; }
    }
}