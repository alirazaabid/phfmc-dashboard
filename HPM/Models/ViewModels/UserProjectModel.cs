﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Models.ViewModels
{
    public class UserProjectModel
    {
        public decimal? Id { get; set; }
        public ProjectModel Project { get; set; }
        public UserModel User { get; set; }
        public ProjectRightModel Right { get; set; }
    }
}