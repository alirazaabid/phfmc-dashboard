﻿using HPM.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Models.ExtensionMethods
{
    public static class ExtensionUtil
    {
        public static IEnumerable<T> Except<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other,
                                                                            Func<T, TKey> getKey)
        {
            return from item in items
                   join otherItem in other on getKey(item)
                   equals getKey(otherItem) into tempItems
                   from temp in tempItems.DefaultIfEmpty()
                   where ReferenceEquals(null, temp) || temp.Equals(default(T))
                   select item;

        }

        public static PaginationOptions<T> WithPaging<T>(this IOrderedQueryable<T> orderedQuery, int page, int pageSize) where T : class
        {
            var totalEntities = orderedQuery.Count();

            var entities = orderedQuery.Skip(page).Take(pageSize).ToList();

            return new PaginationOptions<T>(page, pageSize, totalEntities, entities);
        }



    }
}