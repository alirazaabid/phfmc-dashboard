﻿using HPM.Models;
using HPM.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Services
{
    public class UserProjectService
    {
        public List<UserProjectModel> LoadUserProject()
        {
            var list = new List<UserProjectModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.ViewUserProjectRights.Select(x => new UserProjectModel
                {
                    Id = x.Id,
                    User = new UserModel() { Id = x.UserId, UserName = x.UserName },
                    Project = new ProjectModel() { Id = x.ProjectId, Name = x.ProjectName, Url = x.ProjectUrl },
                    Right = new ProjectRightModel() { Id = x.RightId,Name=x.RightName }
                }).ToList();
            }
            return list;
        }

        public string SaveUserProject(UserProjectModel models,string userId)
        {
            var flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var isExist = db.UserProjectRights.FirstOrDefault(x => x.UserId == models.User.Id && x.ProjectId == models.Project.Id);
                    if (isExist!=null)
                    {
                        flag = Update(models, userId);
                        
                    }
                    else
                    {
                        flag = Save(models, userId);
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }

        private string Update(UserProjectModel models, string userId)
        {
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var userPro = db.UserProjectRights.FirstOrDefault(x => x.UserId == models.User.Id && x.ProjectId == models.Project.Id);
                    userPro.ProjectId = models.Project.Id;
                    userPro.ProjectRightId = models.Right.Id;
                    userPro.UserId = models.User.Id;
                    userPro.UpdatedBy = userId;
                    userPro.UpdatedDate = DateTime.UtcNow.AddHours(5);
                    db.SaveChanges();
                }
                return "update";
            }
            catch (Exception ex)
            {
                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }

        private string Save(UserProjectModel models, string userId)
        {
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var userProject = new UserProjectRight()
                    {
                        ProjectId = models.Project.Id,
                        ProjectRightId = models.Right.Id,
                        UserId = models.User.Id,
                        CreatedBy = userId,
                        CreatedDate = DateTime.UtcNow.AddHours(5)
                    };
                    db.UserProjectRights.Add(userProject);
                    db.SaveChanges();
                }
                return "save";
            }
            catch (Exception ex)
            {
                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }

        }

        public string DeleteUserProject(decimal? id)
        {
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var pro = db.UserProjectRights.FirstOrDefault(x => x.Id == id);
                    if (pro!=null)
                    {
                        db.UserProjectRights.Remove(pro);
                        db.SaveChanges();
                    }
                    return "true";
                }
            }
            catch (Exception ex)
            {
                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
            
        }
    }
}