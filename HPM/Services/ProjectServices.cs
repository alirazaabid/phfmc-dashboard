﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HPM.Models;
using HPM.Models.ViewModels;
using HPM.Models.ExtensionMethods;
namespace HPM.Services
{
    public class ProjectServices
    {
        public List<ProjectModel> LoadProject()
        {
            var list = new List<ProjectModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Projects.OrderByDescending(x=>x.Id).Select(x => new ProjectModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Url = x.Url,
                    Description = x.Description,
                    UserName = x.UserName,
                    Password = x.Password
                }).ToList(); ;
            }
            return list;
        }

        public ProjectModel LoadProjectDetail(decimal? id)
        {
            ProjectModel pm = new ProjectModel();
            using (var db = new HPMDBEntities())
            {
                var pro = db.Projects.FirstOrDefault(x => x.Id == id);
                if (pro != null)
                {
                    pm.Id = pro.Id;
                    pm.Name = pro.Name;
                    pm.Password = pro.Password;
                    pm.UserName = pro.UserName;
                    pm.Description = pro.Description;
                    pm.Url = pro.Url;
                    pm.RequestType = pro.RequestType;
                    pm.ProjectOrder = pro.ProjectOrder;
                    pm.ImageName = pro.Image;
                    pm.Technology = new TechnologyModel()
                    {
                        Id= pro.TechnologyId,
                        Name = pro.Technology?.Name
                    };
                    pm.Program = new ProgramModel()
                    {
                        Id = pro.ProgramId,
                        Name = pro.Program?.Name
                    };
                    var proDeveloper = pro.ProjectDevelopers;
                    var teamLead = proDeveloper.FirstOrDefault(x => x.DeveloperStatus == "Team Lead");
                    if (teamLead != null)
                    {
                        pm.TeamLead = new DeveloperModel()
                        {
                            Id = teamLead?.DeveloperId,
                            Name = teamLead.Developer.Name
                        };
                    }
                    var developer = proDeveloper.Where(x => x.DeveloperStatus == "Developer").ToList();
                    foreach (var item in developer)
                    {
                        var dev = new DeveloperModel()
                        {
                            Id = item.DeveloperId,
                            Name = item.Developer?.Name
                        };
                        pm.Developer.Add(dev);
                    }
                    foreach (var item in pro.ProjectIterations)
                    {
                        var proIteration = new ProjectIterationModel()
                        {
                            Id = item.Id,
                            Name = item.Name,
                            StartDate = item.StartDate,
                            EndDate = item.EndDate,
                            Description = item.Description
                        };
                        pm.ProjectPhases.Add(proIteration);
                    }

                }
            }
            return pm;
        }

        public string SaveProject(ProjectModel models, string userId)
        {
            string flag = "false";
            using (var db = new HPMDBEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var project = new Project()
                        {
                            Name = models.Name,
                            Url = models.Url,
                            Description = models.Description,
                            UserName = models.UserName,
                            TechnologyId = models.Technology?.Id,
                            ProgramId = models.Program?.Id,
                            Password = models.Password,
                            IsEnable = true,
                            RequestType = models.RequestType,
                            ProjectOrder = models.ProjectOrder,
                            Image = models.ImageName,
                            CreatedBy = userId,
                            CreatedDate = DateTime.UtcNow.AddHours(5)
                        };
                        db.Projects.Add(project);
                        var fg = db.SaveChanges();
                        if (fg != 0 && models.TeamLead != null)
                        {
                            var dev = new ProjectDeveloper()
                            {
                                ProjectId = project.Id,
                                DeveloperId = models.TeamLead?.Id,
                                DeveloperStatus = "Team Lead"
                            };
                            db.ProjectDevelopers.Add(dev);
                        }
                        if (fg != 0 && models.Developer.Count() != 0)
                        {
                            foreach (var item in models.Developer)
                            {
                                var dev = new ProjectDeveloper()
                                {
                                    ProjectId = project.Id,
                                    DeveloperId = item.Id,
                                    DeveloperStatus = "Developer"
                                };
                                db.ProjectDevelopers.Add(dev);
                            }
                        }
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return "Exception " + ex.InnerException + " Message " + ex.Message;
                    }
                }


            }
            flag = "true";
            return flag;

        }

        public string EditProject(ProjectModel models, string userId)
        {
            var flag = "false";

            using (var db = new HPMDBEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var pro = db.Projects.FirstOrDefault(x => x.Id == models.Id);
                        if (pro != null)
                        {
                            pro.Name = models.Name;
                            pro.Url = models.Url;
                            pro.Description = models.Description;
                            pro.UserName = models.UserName;
                            pro.Password = models.Password;
                            pro.TechnologyId = models.Technology?.Id;
                            pro.ProgramId = models.Program?.Id;
                            pro.RequestType = models.RequestType;
                            pro.ProjectOrder = models.ProjectOrder;
                            if (!string.IsNullOrEmpty(models.ImageName))
                            {
                                pro.Image = models.ImageName;
                            }
                            pro.UpdatedBy = userId;
                            pro.UpdatedDate = DateTime.UtcNow.AddHours(5);
                           
                            var projectDeveloper = pro.ProjectDevelopers;
                            var teamLead = projectDeveloper.FirstOrDefault(x => x.DeveloperStatus == "Team Lead");
                            if (teamLead != null)
                            {
                                teamLead.DeveloperId = models.TeamLead?.Id;
                            }
                            var developers = projectDeveloper.Where(x => x.DeveloperStatus == "Developer").ToList();
                            var dev = models.Developer.Select(x => new ProjectDeveloper
                            {
                                DeveloperId = (decimal)x.Id,
                            }).ToList();
                            var deletedPro = developers.Except(dev,ct=>ct.DeveloperId).ToList();
                            var addedPro = dev.Except(developers, c => c.DeveloperId).ToList();
                            foreach (var item in deletedPro)
                            {
                                var prodev = pro.ProjectDevelopers.FirstOrDefault(x => x.Id == item.Id);
                               // pro.ProjectDevelopers.Remove(prodev);
                            }
                            deletedPro.ForEach(x => pro.ProjectDevelopers.Remove(x));
                            foreach (var item in addedPro)
                            {
                                var addPro = new ProjectDeveloper()
                                {
                                    DeveloperId = item.DeveloperId,
                                    ProjectId = pro.Id,
                                    DeveloperStatus = "Developer"
                                };
                                pro.ProjectDevelopers.Add(addPro);

                            }
                            db.SaveChanges();
                            transaction.Commit();
                            flag = "true";
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return "Exception " + ex.InnerException + " Message " + ex.Message;
                    }
                }


            }
            return flag;

        }
    }
}