﻿using HPM.Models;
using HPM.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Services
{
    public class DropDownService
    {
        /// <summary>
        ///  load over all rights which we assign to project against user
        /// </summary>
        /// <returns></returns>
        public List<DropDownModel> LoadProjectRights()
        {
            var list = new List<DropDownModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.ProjectRights.Select(x => new DropDownModel
                {
                    Id = x.Id + "",
                    Name = x.Name
                }).ToList();
            }
            return list;
        }

        /// <summary>
        /// load all project 
        /// </summary>
        /// <returns></returns>

        public List<DropDownModel> LoadProjects()
        {
            var list = new List<DropDownModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Projects.Where(x=>x.IsEnable == true).Select(x => new DropDownModel
                {
                    Id = x.Id + "",
                    Name = x.Name
                }).ToList();
            }
            return list;
        }

        /// <summary>
        /// load all users
        /// </summary>
        /// <returns></returns>

        public List<DropDownModel> LoadUsers()
        {
            var list = new List<DropDownModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.AspNetUsers.Select(x => new DropDownModel
                {
                    Id = x.Id,
                    Name = x.UserName
                }).ToList();
            }

            return list;
        }


        /// <summary>
        /// load all technologies to which project are develop.
        /// </summary>
        /// <returns></returns>
        public List<DropDownModel> LoadTechnologies()
        {
            var list = new List<DropDownModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Technologies.Where(x=>x.IsEnable == true).Select(x => new DropDownModel
                {
                    Id = x.Id +"",
                    Name = x.Name
                }).ToList();
            }

            return list;
        }

        /// <summary>
        /// load all developer which work on projects.
        /// </summary>
        /// <returns></returns>
        public List<DropDownModel> LoadDevelopers()
        {
            var list = new List<DropDownModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Developers.Where(x=>x.IsTeamLead != true && x.IsEnable == true).Select(x => new DropDownModel
                {
                    Id = x.Id + "",
                    Name = x.Name
                }).ToList();
            }

            return list;
        }

        /// <summary>
        /// load all team leads which work on projects.
        /// </summary>
        /// <returns></returns>
        /// 
        public List<DropDownModel> LoadTeamLeads()
        {
            var list = new List<DropDownModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Developers.Where(x => x.IsTeamLead == true && x.IsEnable == true).Select(x => new DropDownModel
                {
                    Id = x.Id + "",
                    Name = x.Name
                }).ToList();
            }

            return list;
        }

        /// <summary>
        /// load designation of developers.
        /// </summary>
        /// <returns></returns>
        /// 
        public List<DropDownModel> LoadDesignations()
        {
            var list = new List<DropDownModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Designations.Where(x => x.IsEnable == true).Select(x => new DropDownModel
                {
                    Id = x.Id + "",
                    Name = x.Name
                }).ToList();
            }

            return list;
        }

        /// <summary>
        /// load all program of projects.
        /// </summary>
        /// <returns></returns>
        /// 
        public List<DropDownModel> LoadPrograms()
        {
            var list = new List<DropDownModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Programs.Where(x => x.IsEnable == true).Select(x => new DropDownModel
                {
                    Id = x.Id + "",
                    Name = x.Name
                }).ToList();
            }

            return list;
        }
    }
}