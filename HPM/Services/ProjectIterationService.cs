﻿using HPM.Models;
using HPM.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Services
{
    public class ProjectIterationService
    {
        public List<ProjectIterationModel> LoadProjectIteration()
        {
            var list = new List<ProjectIterationModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.ProjectIterations.Where(x => x.IsEnable == true).OrderByDescending(x => x.Id).Select(x => new ProjectIterationModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Project = new ProjectModel
                    {
                        Id = (decimal)x.ProjectId,
                        Name = x.Project.Name ?? ""
                    },
                    Description = x.Description,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                }).ToList();
            }
            return list;
        }

        public ProjectIterationModel LoadProjectIterationDetail(decimal? id)
        {
            ProjectIterationModel model = new ProjectIterationModel();
            using (var db = new HPMDBEntities())
            {
                var proIteration = db.ProjectIterations.FirstOrDefault(x => x.Id == id);
                if (proIteration != null)
                {
                    model.Id = proIteration.Id;
                    model.Name = proIteration.Name;
                    model.Description = proIteration.Description;
                    model.StartDate = proIteration.StartDate;
                    model.EndDate = proIteration.EndDate;
                    model.Project = new ProjectModel()
                    {
                        Id = (decimal)proIteration.ProjectId,
                        Name = proIteration.Project?.Name

                    };
                }
            }
            return model;
        }

        public string SaveProjectIteration(ProjectIterationModel models, string userId)
        {
            string flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var isExist = db.ProjectIterations.FirstOrDefault(x => x.Name.ToLower() == models.Name.ToLower() &&x.ProjectId == models.Project.Id && x.IsEnable == true);
                    if (isExist == null)
                    {
                        var proIteration = new ProjectIteration()
                        {
                            Name = models.Name,
                            Description = models.Description,
                            StartDate = models.StartDate,
                            EndDate = models.EndDate,
                            ProjectId = models.Project?.Id,
                            IsEnable = true,
                            CreatedBy = userId,
                            CreatedDate = DateTime.UtcNow.AddHours(5)
                        };
                        db.ProjectIterations.Add(proIteration);
                        db.SaveChanges();
                        flag = "true";
                    }
                    else
                    {
                        flag = "project phases name is already exist.";
                    }

                }

                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }

        public string EditProjectIteration(ProjectIterationModel models, string userId)
        {
            var flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var proIteration = db.ProjectIterations.FirstOrDefault(x => x.Id == models.Id);
                    if (proIteration != null)
                    {
                        proIteration.Name = models.Name;
                        proIteration.Description = models.Description;
                        proIteration.StartDate = models.StartDate;
                        proIteration.EndDate = models.EndDate;
                        proIteration.ProjectId = models.Project?.Id;
                        proIteration.UpdatedBy = userId;
                        proIteration.UpdatedDate = DateTime.UtcNow.AddHours(5);
                        db.SaveChanges();
                        flag = "true";
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }
    }
}