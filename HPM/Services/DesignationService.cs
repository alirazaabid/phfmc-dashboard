﻿using HPM.Models;
using HPM.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Services
{
    public class DesignationService
    {
        public List<DesignationModel> LoadDesignations()
        {
            var list = new List<DesignationModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Designations.Where(x => x.IsEnable == true).OrderByDescending(x => x.Id).Select(x => new DesignationModel
                {
                    Id = x.Id,
                    Name = x.Name,
                   Description = x.Description
                }).ToList();
            }
            return list;
        }

        public DesignationModel LoadDesignationDetail(decimal? id)
        {
            DesignationModel des = new DesignationModel();
            using (var db = new HPMDBEntities())
            {
                var de = db.Designations.FirstOrDefault(x => x.Id == id);
                if (de != null)
                {
                    des.Id = de.Id;
                    des.Name = de.Name;
                    des.Description = de.Description;
                }
            }
            return des;
        }

        public string SaveDesignation(DesignationModel models, string userId)
        {
            string flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var isExist = db.Designations.FirstOrDefault(x => x.Name.ToLower() == models.Name.ToLower() && x.IsEnable == true);
                    if (isExist==null)
                    {
                        var designation = new Designation()
                        {
                            Name = models.Name,
                            Description = models.Description,
                            IsEnable = true,
                            CreatedBy = userId,
                            CreatedDate = DateTime.UtcNow.AddHours(5)
                        };
                        db.Designations.Add(designation);
                        db.SaveChanges();
                        flag = "true";
                    }
                    else
                    {
                        flag = "Designation already exist.";
                    }
                    
                }
                
                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }

        public string EditDesignation(DesignationModel models, string userId)
        {
            var flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var des = db.Designations.FirstOrDefault(x => x.Id == models.Id);
                    if (des != null)
                    {
                        des.Name = models.Name;
                        des.Description = models.Description;
                        des.UpdatedBy = userId;
                        des.UpdatedDate = DateTime.UtcNow.AddHours(5);
                        db.SaveChanges();
                        flag = "true";
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }
    }
}