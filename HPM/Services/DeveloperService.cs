﻿using HPM.Models;
using HPM.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Services
{
    public class DeveloperService
    {

        public List<DeveloperModel> LoadDevelopers()
        {
            var list = new List<DeveloperModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Developers.Where(x=>x.IsEnable == true).OrderByDescending(x=>x.Id).Select(x => new DeveloperModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    LastName = x.LastName,
                    Description = x.Description,
                    CNIC = x.CNIC,
                    Address = x.Address,
                    ContactNo = x.ContactNo,
                    TeamLead = x.IsTeamLead,
                    Designation = new DesignationModel {
                        Id = x.Designation.Id,
                        Name = x.Designation.Name
                    }
                }).ToList();
            }
            return list;
        }

        public DeveloperModel LoadDeveloperDetail(decimal? id)
        {
            DeveloperModel dv = new DeveloperModel();
            using (var db = new HPMDBEntities())
            {
                var dev = db.Developers.FirstOrDefault(x => x.Id == id);
                if (dev != null)
                {
                    dv.Id = dev.Id;
                    dv.Name = dev.Name;
                    dv.LastName = dev.LastName;
                    dv.Description = dev.Description;
                    dv.ContactNo = dev.ContactNo;
                    dv.CNIC = dev.CNIC;
                    dv.Address = dev.Address;
                    dv.TeamLead = dev?.IsTeamLead;
                    dv.Designation = new DesignationModel
                    {
                        Id = dev.Designation?.Id,
                        Name = dev.Designation?.Name
                    };
                }
            }
            return dv;
        }

        public string SaveDeveloper(DeveloperModel models, string userId)
        {
            string flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var developer = new Developer()
                    {
                        Name = models.Name,
                        LastName = models.LastName,
                        Description = models.Description,
                        ContactNo = models.ContactNo,
                        CNIC = models.CNIC,
                        Address = models.Address,
                        IsTeamLead = models.TeamLead,
                        DesignationId = models.Designation?.Id,
                        IsEnable = true,
                        CreatedBy = userId,
                        CreatedDate = DateTime.UtcNow.AddHours(5)
                    };
                    db.Developers.Add(developer);
                    db.SaveChanges();
                }
                flag = "true";
                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }

        public string EditProject(DeveloperModel models, string userId)
        {
            var flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var dev = db.Developers.FirstOrDefault(x => x.Id == models.Id);
                    if (dev != null)
                    {
                        dev.Name = models.Name;
                        dev.LastName = models.LastName;
                        dev.Description = models.Description;
                        dev.ContactNo = models.ContactNo;
                        dev.CNIC = models.CNIC;
                        dev.Address = models.Address;
                        dev.IsTeamLead = models.TeamLead;
                        dev.DesignationId = models.Designation?.Id;
                        dev.UpdatedBy = userId;
                        dev.UpdatedDate = DateTime.UtcNow.AddHours(5);
                        db.SaveChanges();
                        flag = "true";
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }

    }
}