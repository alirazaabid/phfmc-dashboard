﻿using HPM.Models;
using HPM.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPM.Services
{
    public class ProgramService
    {
        public List<ProgramModel> LoadPrograms()
        {
            var list = new List<ProgramModel>();
            using (var db = new HPMDBEntities())
            {
                list = db.Programs.Where(x => x.IsEnable == true).OrderByDescending(x => x.Id).Select(x => new ProgramModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();
            }
            return list;
        }

        public ProgramModel LoadProgramDetail(decimal? id)
        {
            ProgramModel model = new ProgramModel();
            using (var db = new HPMDBEntities())
            {
                var pro = db.Programs.FirstOrDefault(x => x.Id == id);
                if (pro != null)
                {
                    model.Id = pro.Id;
                    model.Name = pro.Name;
                    model.Description = pro.Description;
                }
            }
            return model;
        }

        public string SaveProgram(ProgramModel model, string userId)
        {
            string flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var isExist = db.Programs.FirstOrDefault(x => x.Name.ToLower() == model.Name.ToLower() && x.IsEnable == true);
                    if (isExist==null)
                    {
                        var pro = new Program()
                        {
                            Name = model.Name,
                            Description = model.Description,
                            IsEnable = true,
                            CreatedBy = userId,
                            CreatedDate = DateTime.UtcNow.AddHours(5)
                        };
                        db.Programs.Add(pro);
                        db.SaveChanges();
                        flag = "true";
                    }
                    else
                    {
                        flag = "Program is already added.";
                    }
                    
                }
               
                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }

        public string EditProgram(ProgramModel model, string userId)
        {
            var flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var pro = db.Programs.FirstOrDefault(x => x.Id == model.Id);
                    if (pro != null)
                    {
                        pro.Name = model.Name;
                        pro.Description = model.Description;
                        pro.UpdatedBy = userId;
                        pro.UpdatedDate = DateTime.UtcNow.AddHours(5);
                        db.SaveChanges();
                        flag = "true";
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }

        public string DisableProgram(decimal? id)
        {
            var flag = "false";
            try
            {
                using (var db = new HPMDBEntities())
                {
                    var pro = db.Programs.FirstOrDefault(x => x.Id == id);
                    if (pro!=null)
                    {
                        pro.IsEnable = false;
                    }
                    db.SaveChanges();
                    flag = "true";
                }
                return flag;
            }
            catch (Exception ex)
            {

                return "Exception " + ex.InnerException + " Message " + ex.Message;
            }
        }

        //public List<ProgramModel> LoadProjects()
        //{
        //    var list = new List<ProgramModel>();
        //    using (var db = new HPMDBEntities())
        //    {
        //        list = db.Programs.Where(x => x.IsEnable == true).OrderByDescending(x => x.Id).Select(x => new ProgramModel
        //        {
        //            Id = x.Id,
        //            Name = x.Name,
        //            Description = x.Description
        //        }).ToList();
        //    }
        //    return list;
        //}
    }
}