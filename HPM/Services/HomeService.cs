﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HPM.Models;
using HPM.Models.ViewModels;
using System.Text;

namespace HPM.Services
{
    public class HomeService
    {
        public int ProjectCount()
        {
            var count = 0;
            using (var db = new HPMDBEntities())
            {
                count = db.Projects.Count();
            }
            return count;
        }

        public int UserCount()
        {
            var count = 0;
            using (var db = new HPMDBEntities())
            {
                count = db.AspNetUsers.Count();
            }
            return count;
        }

        public int TeamLeadCount()
        {
            var count = 0;
            using (var db = new HPMDBEntities())
            {
                count = db.Developers.Where(x=>x.IsEnable == true && x.IsTeamLead == true).Count();
            }
            return count;
        }

        public int DeveloperCount()
        {
            var count = 0;
            using (var db = new HPMDBEntities())
            {
                count = db.Developers.Where(x => x.IsEnable == true && x.IsTeamLead != true).Count();
            }
            return count;
        }

        public List<UserProjectModel> LoadUserProject(string userId)
        {
            var list = new List<UserProjectModel>();
            using (var db = new HPMDBEntities())
            {
                var userProjectList = db.ViewUserProjectRights.Where(x => x.UserId == userId).OrderBy(x=>x.ProjectOrder).ToList();
                list = userProjectList.Select(x => new UserProjectModel
                {
                    Id = x.Id,
                    User = new UserModel() { Id = x.UserId, UserName = x.UserName },
                    Project = new ProjectModel()
                    {
                        Id = x.ProjectId,
                        Name = x.ProjectName,
                        Url = x.ProjectUrl,
                        UserNameBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(x.ProjectUserName ?? "")),
                        PasswordBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(x.ProjectPassword ?? "")),
                        UserName = x.ProjectUserName,
                        Password = x.ProjectPassword,
                        ImageName = x.ImageUrl,
                        RequestType = x.RequestType,
                        Description = x.ProjectDescription

                    },
                    Right = new ProjectRightModel() { Id = x.RightId, Name = x.RightName }
                }).ToList();
            }
            return list;
        }
    }
}